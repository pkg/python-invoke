python-invoke (2.0.0-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Apr 2023 23:10:40 +0000

python-invoke (2.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Remove patch unvendorize-six-and-yaml.patch, fixed upstream

 -- Antoine Beaupré <anarcat@debian.org>  Thu, 26 Jan 2023 15:17:00 -0500

python-invoke (1.7.0+ds-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Update standards version to 4.6.0, no changes needed.
  * Set upstream metadata fields: Repository.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 22 Nov 2022 01:33:09 +0000

python-invoke (1.7.0+ds-1) unstable; urgency=medium

  * Upload experimental version to unstable, unchanged.

 -- Antoine Beaupré <anarcat@debian.org>  Wed, 27 Apr 2022 12:11:52 -0400

python-invoke (1.7.0+ds-1~exp1) experimental; urgency=medium

  * New upstream release.

 -- Antoine Beaupré <anarcat@debian.org>  Thu, 31 Mar 2022 21:14:38 -0400

python-invoke (1.5.0+ds-1) unstable; urgency=medium

  * Acknowledge recent NMus.
  * Salvage package: transfer maintainership to Python team, keeping zigo,
    adding myself, as uploaders. (Closes: #964718)
  * New upstream release.
  * Bump standards, no change.
  * Switch to sane (DEP-14) git branch layout instead of
    Openstack-specific one.

 -- Antoine Beaupré <anarcat@debian.org>  Thu, 04 Mar 2021 20:34:57 -0500

python-invoke (1.4.1+ds-0.1apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 21 Apr 2021 11:17:40 +0200

python-invoke (1.4.1+ds-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream release.
  * Fix uscan configuration to automatically repack orig tarball.

 -- Antoine Beaupré <anarcat@debian.org>  Wed, 08 Jul 2020 15:59:58 -0400

python-invoke (1.3.0+ds-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * New upstream version 1.3.0+ds. For a list of changes, see:
    https://www.pyinvoke.org/changelog.html (Closes: #935108)
  * Add runtime dependency to python3-pkg-resources.
  * Refresh unvendorize-six-and-yaml.patch for new upstream version.

 -- Luca Boccassi <bluca@debian.org>  Sat, 23 Nov 2019 16:46:49 +0000

python-invoke (0.11.1+dfsg1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/control: Use team+openstack@tracker.debian.org as maintainer
  * Use debhelper-compat instead of debian/compat.
  * d/control: Set Vcs-* to salsa.debian.org

  [ Daniel Baumann ]
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * Removed Python 2 support (Closes: #937840).

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Sep 2019 17:54:41 +0200

python-invoke (0.11.1+dfsg1-1) unstable; urgency=medium

  * New upstream release:
    - Fixes FTBFS (Closes: #802894).
  * Rewrote unvendorize-six.patch.
  * Switched to git tags packaging workflow.
  * Fixed watch file to use github tag rather than broken pypi.
  * Removed unit testing, because upstream now uses spec, which isn't in
    Debian.
  * Using HTTPS for VCS URLs.
  * Fix debian/copyright ordering, years, and six.py removal.

 -- Thomas Goirand <zigo@debian.org>  Wed, 28 Oct 2015 06:00:18 +0000

python-invoke (0.9.0-2) unstable; urgency=medium

  * override_dh_python3 to fix Py3 shebang.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Oct 2015 00:04:28 +0000

python-invoke (0.9.0-1) unstable; urgency=medium

  * Initial release. (Closes: #768952)

 -- Thomas Goirand <zigo@debian.org>  Mon, 10 Nov 2014 19:00:56 +0800
